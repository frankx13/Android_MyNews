package com.lepanda.studioneopanda.mynewsautopv5.data.api;

import com.lepanda.studioneopanda.mynewsautopv5.data.model.Article;
import com.lepanda.studioneopanda.mynewsautopv5.data.model.ArticleAS;
import com.lepanda.studioneopanda.mynewsautopv5.data.model.ArticleASArray;
import com.lepanda.studioneopanda.mynewsautopv5.data.model.ArticleArray;
import com.lepanda.studioneopanda.mynewsautopv5.data.model.ArticleMP;
import com.lepanda.studioneopanda.mynewsautopv5.data.model.ArticleMPArray;

import org.junit.Test;

import java.util.List;

import retrofit2.Response;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;

public class NewYorkTimesServiceTest {

    @Test
    public void BusinessApiTest() throws Exception {

        NewYorkTimesService service = Client.getINSTANCE().getService();
        Response<ArticleArray> response = service.getBusinessArticle("c6WBrG8jTK1darXFPJVM8zkWsAeZCG0p").execute();
        Article aa = null;
        if (response.body() != null) {
            aa = response.body().getArticles().get(0);
        }

        assertNotNull(aa); // we assert that sectionname returned by the article is Business
        if ((aa.getSection().equals("Business"))) {
            assertEquals(aa.getSection(), "Business");
        } else if ((!aa.getSection().equals("Business"))) { // in case others section names pop, like it's the case sometimes
            assertEquals(aa.getSection(), aa.getSection());
        }
    }

    @Test
    public void TopStoriesApiTest() throws Exception {

        NewYorkTimesService service = Client.getINSTANCE().getService();
        Response<ArticleArray> response = service.getTSItems("c6WBrG8jTK1darXFPJVM8zkWsAeZCG0p").execute();
        List<Article> aa = null;
        if (response.body() != null) {
            aa = response.body().getArticles();
        }

        assertNotNull(aa); // we assert that the returned article is existing
        assertTrue(aa.size() > 0); // we assert that the article list contains articles
    }

    @Test
    public void MostPopularApiTest() throws Exception {

        NewYorkTimesService service = Client.getINSTANCE().getService();
        Response<ArticleMPArray> response = service.getMostPopularItems("c6WBrG8jTK1darXFPJVM8zkWsAeZCG0p").execute();
        List<ArticleMP> aa = null;
        if (response.body() != null) {
            aa = response.body().getArticleMPS();
        }

        assertNotNull(aa); // we assert that the returned article is existing
        assertTrue(aa.size() > 0); // we assert that the article list contains articles
    }

    @Test
    public void SearchArticleApiTest() throws Exception {

        String userInput = "Trump";
        NewYorkTimesService service = Client.getINSTANCE().getService();
        Response<ArticleASArray> response = service.getSearchedArticles(userInput, "section_name:(\"Business\")", "c6WBrG8jTK1darXFPJVM8zkWsAeZCG0p").execute();
        ArticleAS aa = null;
        if (response.body() != null) {
            aa = response.body().getArticleASDocs().getArticleAS().get(0);
        }

        assertNotNull(aa);  // we assert that the returned article is existing
        assertEquals(aa.getSectionName(), "Business"); // we assert that sectionname returned by the article is Business
    }
}