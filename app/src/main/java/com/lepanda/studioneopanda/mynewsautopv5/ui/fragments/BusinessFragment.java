package com.lepanda.studioneopanda.mynewsautopv5.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lepanda.studioneopanda.mynewsautopv5.data.model.Article;
import com.lepanda.studioneopanda.mynewsautopv5.data.model.ArticleArray;
import com.lepanda.studioneopanda.mynewsautopv5.data.api.Client;
import com.lepanda.studioneopanda.mynewsautopv5.data.api.NewYorkTimesService;
import com.lepanda.studioneopanda.mynewsautopv5.R;
import com.lepanda.studioneopanda.mynewsautopv5.ui.adapters.RecyclerViewAdapter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BusinessFragment extends Fragment {

    private RecyclerView recyclerView;
    private List<Article> listArticle;


    public BusinessFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.business_fragment, container, false);
        recyclerView = v.findViewById(R.id.business_recyclerview);
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData();
    }

    private void loadData() {
        Client client = Client.getINSTANCE();
        NewYorkTimesService service = client.getService();
        Call<ArticleArray> call = service.getBusinessArticle((getString(R.string.api_key)));

        call.enqueue(new Callback<ArticleArray>() {
            @Override
            public void onResponse(@NonNull Call<ArticleArray> call, @NonNull Response<ArticleArray> response) {
                ArticleArray articleArray = response.body();

                if (articleArray != null) {
                    listArticle = articleArray.getArticles();
                }

                onDataLoaded();
            }

            @Override
            public void onFailure(@NonNull Call<ArticleArray> call, @NonNull Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
    }

    private void onDataLoaded() {
        RecyclerViewAdapter recyclerAdapter = new RecyclerViewAdapter(getContext(), listArticle);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(recyclerAdapter);
    }
}

