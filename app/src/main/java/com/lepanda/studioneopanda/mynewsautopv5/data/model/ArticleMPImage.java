package com.lepanda.studioneopanda.mynewsautopv5.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ArticleMPImage {

    @SerializedName("url")
    @Expose
    private String url;

    //CONSTRUCTOR
    public ArticleMPImage(String url) {
        this.url = url;
    }

    //GETTER
    public String getUrl() {
        return url;
    }
}