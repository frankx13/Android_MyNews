package com.lepanda.studioneopanda.mynewsautopv5.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ArticleMPArray {

    @SerializedName("results")
    @Expose
    private List<ArticleMP> articleMPS;

    //GETTER
    public List<ArticleMP> getArticleMPS() {
        return articleMPS;
    }
}