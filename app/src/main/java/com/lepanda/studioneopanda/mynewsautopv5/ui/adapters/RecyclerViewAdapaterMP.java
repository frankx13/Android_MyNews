package com.lepanda.studioneopanda.mynewsautopv5.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lepanda.studioneopanda.mynewsautopv5.data.model.ArticleMP;
import com.lepanda.studioneopanda.mynewsautopv5.ui.activities.DetailActivity;
import com.lepanda.studioneopanda.mynewsautopv5.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RecyclerViewAdapaterMP extends RecyclerView.Adapter<RecyclerViewAdapaterMP.MyViewHolderMP> {

    private Context mContext;
    private List<ArticleMP> mDataMP;

    //CONSTRUCTOR
    public RecyclerViewAdapaterMP(Context mContext, List<ArticleMP> mDataMP) {
        this.mContext = mContext;
        this.mDataMP = mDataMP;
    }

    //VIEWHOLDER
    @NonNull
    @Override
    public MyViewHolderMP onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v;
        //We use this layout to display items inside
        v = LayoutInflater.from(mContext).inflate(R.layout.item_list, viewGroup, false);
        return new MyViewHolderMP(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolderMP holderMP, int position) {
        final ArticleMP a = mDataMP.get(position);

        holderMP.title_articleMP.setText(mDataMP.get(position).getSection()); //return the section name
        holderMP.resume_articleMP.setText(mDataMP.get(position).getResume()); //return the resume of an article

        if (mDataMP.get(position).getMedia() != null && mDataMP.get(position).getMedia().size() > 0) { //load image from article
            Picasso.with(this.mContext)
                    .load((mDataMP.get(position).getMedia().get(0).getMediaMetadata().get(0).getUrl()))
                    .placeholder(R.drawable.loading)
                    .into(holderMP.img_articleMP);
        } else {
            holderMP.img_articleMP.setImageResource(R.drawable.loading); //if image not found, we load a local image instead
        }

        holderMP.article_item_layout.setOnClickListener(new View.OnClickListener() { // when click on an article, we send the URL to
                                                                                     // the Detail Activity for it to load the original article,
                                                                                     // with the OnClickListener affected to the LinearLayout
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, DetailActivity.class);
                intent.putExtra("URL_ARTICLE", a.getUrl());
                mContext.startActivity(intent);
            }
        });
    }

    // we return the size of the article list
    @Override
    public int getItemCount() {
        return ((mDataMP == null) ? 0 : mDataMP.size());
    }

    static class MyViewHolderMP extends RecyclerView.ViewHolder {
        // declaration of UI Elements
        private LinearLayout article_item_layout;
        private TextView title_articleMP;
        private TextView resume_articleMP;
        private ImageView img_articleMP;

        MyViewHolderMP(@NonNull View itemView) {
            super(itemView);
            // we assign a view to each UI Element
            article_item_layout = itemView.findViewById(R.id.article_item_layout);
            title_articleMP = itemView.findViewById(R.id.title_article);
            resume_articleMP = itemView.findViewById(R.id.resume_article);
            img_articleMP = itemView.findViewById(R.id.img_article);
        }
    }
}
