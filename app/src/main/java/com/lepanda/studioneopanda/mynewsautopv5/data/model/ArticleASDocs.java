package com.lepanda.studioneopanda.mynewsautopv5.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ArticleASDocs {
    @SerializedName("docs")
    @Expose
    private List<ArticleAS> articleAS;

    //GETTER
    public List<ArticleAS> getArticleAS() {
        return articleAS;
    }
}