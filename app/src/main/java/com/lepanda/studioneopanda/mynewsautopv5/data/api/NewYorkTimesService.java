package com.lepanda.studioneopanda.mynewsautopv5.data.api;

import com.lepanda.studioneopanda.mynewsautopv5.data.model.ArticleASArray;
import com.lepanda.studioneopanda.mynewsautopv5.data.model.ArticleArray;
import com.lepanda.studioneopanda.mynewsautopv5.data.model.ArticleMPArray;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NewYorkTimesService { // We use API with Queries = key value pair to make information reusable and
                                       // in order to be more flexible in our code

    @GET("topstories/v2/business.json?") // Business API
    Call<ArticleArray> getBusinessArticle(@Query("api-key") String key);

    @GET("topstories/v2/home.json?") // TopStories API
    Call<ArticleArray> getTSItems(@Query("api-key") String key);

    @GET("mostpopular/v2/viewed/1.json?") // MostPopular API
    Call<ArticleMPArray> getMostPopularItems(@Query("api-key") String key);

    @GET("search/v2/articlesearch.json?") // SearchArticle API
    Call<ArticleASArray> getSearchedArticles(@Query("q") String query,
                                                     @Query("fq") String filter,
                                                     @Query("api-key") String key);
}
