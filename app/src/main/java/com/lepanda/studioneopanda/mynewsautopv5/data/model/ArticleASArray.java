package com.lepanda.studioneopanda.mynewsautopv5.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ArticleASArray {
    @SerializedName("response")
    @Expose
    private ArticleASDocs articleASDocs;

    //GETTER
    public ArticleASDocs getArticleASDocs() {
        return articleASDocs;
    }

}