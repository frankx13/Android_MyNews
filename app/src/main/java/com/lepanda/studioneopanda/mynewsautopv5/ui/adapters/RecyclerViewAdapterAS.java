package com.lepanda.studioneopanda.mynewsautopv5.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lepanda.studioneopanda.mynewsautopv5.data.model.ArticleAS;
import com.lepanda.studioneopanda.mynewsautopv5.ui.activities.DetailActivity;
import com.lepanda.studioneopanda.mynewsautopv5.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RecyclerViewAdapterAS extends RecyclerView.Adapter<RecyclerViewAdapterAS.MyViewHolderAS> {
    private Context mContext;
    private List<ArticleAS> mDataAS;

    public RecyclerViewAdapterAS(Context mContext, List<ArticleAS> mDataAS) {
        this.mContext = mContext;
        this.mDataAS = mDataAS;
    }

    @NonNull
    @Override
    public RecyclerViewAdapterAS.MyViewHolderAS onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v;
        v = LayoutInflater.from(mContext).inflate(R.layout.item_list, viewGroup, false);
        return new MyViewHolderAS(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapterAS.MyViewHolderAS holderAS, int position) {
        final ArticleAS a = mDataAS.get(position);
        holderAS.resume_articleAS.setText(mDataAS.get(position).getLeadParagraph());
        holderAS.section_articleAS.setText(mDataAS.get(position).getSectionName());

        if (mDataAS.get(position).getMultimedia() != null && mDataAS.get(position).getMultimedia().size() > 0) {
            Picasso.with(this.mContext)
                    .load((mDataAS.get(position).getMultimedia().get(0).getUrl()))
                    .placeholder(R.drawable.loading)
                    .into(holderAS.img_articleAS);
        } else {
            holderAS.img_articleAS.setImageResource(R.drawable.loading);
        }

        holderAS.article_item_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, DetailActivity.class);
                intent.putExtra("URL_ARTICLE", a.getWebUrl());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return ((mDataAS == null) ? 0 : mDataAS.size());
    }

    static class MyViewHolderAS extends RecyclerView.ViewHolder {

        private LinearLayout article_item_layout;
        private TextView section_articleAS;
        private ImageView img_articleAS;
        private TextView resume_articleAS;

        MyViewHolderAS(@NonNull View itemView) {
            super(itemView);
            article_item_layout = itemView.findViewById(R.id.article_item_layout);
            img_articleAS = itemView.findViewById(R.id.img_article);
            resume_articleAS = itemView.findViewById(R.id.resume_article);
            section_articleAS = itemView.findViewById(R.id.title_article);
        }
    }
}