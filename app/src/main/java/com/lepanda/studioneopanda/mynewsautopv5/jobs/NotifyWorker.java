package com.lepanda.studioneopanda.mynewsautopv5.jobs;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.lepanda.studioneopanda.mynewsautopv5.R;
import com.lepanda.studioneopanda.mynewsautopv5.data.api.Client;
import com.lepanda.studioneopanda.mynewsautopv5.data.api.NewYorkTimesService;
import com.lepanda.studioneopanda.mynewsautopv5.data.model.ArticleASArray;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import androidx.work.Worker;
import androidx.work.WorkerParameters;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.lepanda.studioneopanda.mynewsautopv5.applicationclass.MyNewsApp.CHANNEL_1_ID;

public class NotifyWorker extends Worker {

    private Context mContext;
    private String listArticle;
    private SharedPreferences mPreferences;
    private String sharedPrefFile = "";

    //CONSTRUCTOR
    public NotifyWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() { //BACKGROUND TASK

        mContext = getApplicationContext();
        String userInput = "";
        mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        String filterOfAPI = getInputData().getString("FilterOfAPI");




        Client client = Client.getINSTANCE();
        NewYorkTimesService service = client.getService();
        Call<ArticleASArray> call = service.getSearchedArticles(userInput, filterOfAPI, "c6WBrG8jTK1darXFPJVM8zkWsAeZCG0p");
        call.enqueue(new Callback<ArticleASArray>() {
            @SuppressLint("CommitPrefEdits")
            @Override
            public void onResponse(@NonNull Call<ArticleASArray> call, @NonNull Response<ArticleASArray> response) {
                try {
                    ArticleASArray articleASArray = response.body();

                    if (articleASArray != null) {
                        listArticle = articleASArray.getArticleASDocs().getArticleAS().get(0).getPubDate();
                    }
                    sharedPrefFile = listArticle.substring(0, 10);
                    mPreferences.edit().putString("FetchedNews", sharedPrefFile); // We catch the value of today's date
                                                                                  // and save it inside our SharedPrefs
                    //Log.e("VALUEOFPUBDATE", sharedPrefFile); //= 2019-02-02
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ArticleASArray> call, @NonNull Throwable t) {
                Toast.makeText(mContext, "Failure to contact the server", Toast.LENGTH_SHORT).show();
            }
        });

        Date currentDate = Calendar.getInstance().getTime(); //we get the current time
        @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String currentDateToday = df.format(currentDate); // we reformat the today date

        // Log.e("VALUEOFCURRENTDATE", currentDateToday);  = 2019-02-19

        // We compare current date and the date we fetched in the network call, if it's not alike, we create a notification
        // to warn the user about the presence of new articles
        if (!Objects.requireNonNull(mPreferences.getString("FetchedNews", "")).equals(currentDateToday)) {
            android.app.NotificationManager notificationManager =
                    (android.app.NotificationManager) mContext
                            .getSystemService(Context.NOTIFICATION_SERVICE);

            String title = "MyNews";
            String message = "Des articles correspondant à votre recherche ont été publiés.";

            Notification notification = new NotificationCompat.Builder(mContext, CHANNEL_1_ID) // We implement and custom
                                                                                               // the notification box
                    .setSmallIcon(R.drawable.ic_launcher_foreground)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                    .setShowWhen(true)
                    .build();
            if (notificationManager != null) {
                notificationManager.notify(1, notification);
            }
        }
        return Result.success(); // end of the WorkRequest
    }
}