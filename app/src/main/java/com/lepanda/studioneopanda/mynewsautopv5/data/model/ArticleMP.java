package com.lepanda.studioneopanda.mynewsautopv5.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ArticleMP {

    @SerializedName("section")
    @Expose
    private String section;
    @SerializedName("abstract")
    @Expose
    private String resume;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("media")
    @Expose
    private List<Medium> media;

    //CONSTRUCTOR
    public ArticleMP(String section, String _abstract, String url, List<Medium> media) {
        this.section = section;
        this.resume = _abstract;
        this.url = url;
        this.media = media;
    }

    //GETTER
    public String getSection() {
        return section;
    }

    public String getResume() {
        return resume;
    }

    public String getUrl() {
        return url;
    }

    public List<Medium> getMedia() {
        return media;
    }
}
