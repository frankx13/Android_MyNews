package com.lepanda.studioneopanda.mynewsautopv5.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Article {

    @SerializedName("section")
    @Expose
    private String section; // to fetch the section name

    @SerializedName("abstract")
    @Expose
    private String resume; // to fetch the resume name

    @SerializedName("url")
    @Expose
    private String urlArticle; // to fetch the URL of an article

    @SerializedName("multimedia")
    @Expose
    private List<ArticleImage> multimedia; // to fetch the image of an article


    // CONSTRUCTOR
    public Article(String section, String resume, String urlArticle, List<ArticleImage> multimedia) {
        this.section = section;
        this.resume = resume;
        this.urlArticle = urlArticle;
        this.multimedia = multimedia;
    }

    // GETTERS

    public List<ArticleImage> getMultimedia() {
        return multimedia;
    }

    public String getSection() {
        return section;
    }

    public String getResume() {
        return resume;
    }

    public String getUrlArticle() {
        return urlArticle;
    }

}