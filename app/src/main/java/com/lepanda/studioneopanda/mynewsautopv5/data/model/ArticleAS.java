package com.lepanda.studioneopanda.mynewsautopv5.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ArticleAS {

    @SerializedName("section_name")
    @Expose
    private String sectionName;
    @SerializedName("web_url")
    @Expose
    private String webUrl;
    @SerializedName("lead_paragraph")
    @Expose
    private String leadParagraph;
    @SerializedName("multimedia")
    @Expose
    private List<ArticleASImage> multimedia;
    @SerializedName("pub_date")
    @Expose
    private String pubDate;

    //CONSTRUCTOR
    public ArticleAS(String sectionName, String webUrl, String leadParagraph, List<ArticleASImage> multimedia, String pubDate) {
        this.sectionName = sectionName;
        this.webUrl = webUrl;
        this.leadParagraph = leadParagraph;
        this.multimedia = multimedia;
        this.pubDate = pubDate;
    }

    //GETTERS
    public String getPubDate() {
        return pubDate;
    }

    public String getSectionName() {
        return sectionName;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public String getLeadParagraph() {
        return leadParagraph;
    }

    public List<ArticleASImage> getMultimedia() {
        return multimedia;
    }

}