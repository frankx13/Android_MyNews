package com.lepanda.studioneopanda.mynewsautopv5.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lepanda.studioneopanda.mynewsautopv5.data.model.Article;
import com.lepanda.studioneopanda.mynewsautopv5.ui.activities.DetailActivity;
import com.lepanda.studioneopanda.mynewsautopv5.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {

    private Context mContext;
    private List<Article> mData;

    public RecyclerViewAdapter(Context mContext, List<Article> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull final ViewGroup viewGroup, int i) {
        View v;
        v = LayoutInflater.from(mContext).inflate(R.layout.item_list, viewGroup, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final Article a = mData.get(position);
        holder.title_article.setText(mData.get(position).getSection());
        holder.resume_article.setText(mData.get(position).getResume());

        if (mData.get(position).getMultimedia() != null && mData.get(position).getMultimedia().size() > 0) {
            Picasso.with(this.mContext)
                    .load((mData.get(position).getMultimedia().get(0).getUrlImage()))
                    .placeholder(R.drawable.loading)
                    .into(holder.img_article);
        } else {
            holder.img_article.setImageResource(R.drawable.loading);
        }
        holder.article_item_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, DetailActivity.class);
                intent.putExtra("URL_ARTICLE", a.getUrlArticle());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return ((mData == null) ? 0 : mData.size());
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout article_item_layout;
        private TextView title_article;
        private TextView resume_article;
        private ImageView img_article;
        private WebView webview_article;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            article_item_layout = itemView.findViewById(R.id.article_item_layout);
            title_article = itemView.findViewById(R.id.title_article);
            resume_article = itemView.findViewById(R.id.resume_article);
            img_article = itemView.findViewById(R.id.img_article);
            webview_article = itemView.findViewById(R.id.mywebview);
        }
    }
}