package com.lepanda.studioneopanda.mynewsautopv5.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.lepanda.studioneopanda.mynewsautopv5.data.model.ArticleAS;
import com.lepanda.studioneopanda.mynewsautopv5.data.model.ArticleASArray;
import com.lepanda.studioneopanda.mynewsautopv5.data.api.Client;
import com.lepanda.studioneopanda.mynewsautopv5.data.api.NewYorkTimesService;
import com.lepanda.studioneopanda.mynewsautopv5.R;
import com.lepanda.studioneopanda.mynewsautopv5.ui.adapters.RecyclerViewAdapterAS;

import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArticleSearchFragment extends Fragment {
    private View v; // view we will use in the fragment

    private RecyclerView recyclerViewAs;

    private List<ArticleAS> listArticle;

    public ArticleSearchFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.search_results, container, false);

        recyclerViewAs = v.findViewById(R.id.search_recyclerview); // initialize the RV

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        loadData(); // network call is called
    }


    private void loadData() {
        // receive EditText input
        String userInput = null;
        if (getArguments() != null) {
            userInput = getArguments().getString("UserInput");
        }

        Client client = Client.getINSTANCE();
        NewYorkTimesService service = client.getService();

        boolean isBusiness;
        boolean isArts;
        boolean isSports;
        boolean isOpinion;
        boolean isMovies;
        boolean isTravel;
        if (getArguments() != null) { // in function of the checkbox ticked, we adapt the proper filter

            isBusiness = getArguments().getBoolean("BusinessBoxCheck"); // getting the boolean corresponding to which box has been checked
            isArts = getArguments().getBoolean("ArtsBoxCheck");
            isSports = getArguments().getBoolean("SportsBoxCheck");
            isOpinion = getArguments().getBoolean("OpinionBoxCheck");
            isMovies = getArguments().getBoolean("MoviesBoxCheck");
            isTravel = getArguments().getBoolean("TravelBoxCheck");
            String filterFQ = "";

            if (isBusiness) {
                filterFQ = "section_name:(\"Business\")"; // filter to get section Business if BusinessBox checked in the other search fragment
            } else if (isArts) {
                filterFQ = "section_name:(\"Arts\")"; // filter to get section Science if ScienceBox checked in the other search fragment
            } else if (isSports) {
                filterFQ = "section_name:(\"Sports\")"; // filter to get section Sports if Sports checked in the other search fragment
            }  else if (isOpinion) {
                filterFQ = "section_name:(\"Opinion\")"; // filter to get section Opinion if Sports checked in the other search fragment
            } else if (isTravel) {
                filterFQ = "section_name:(\"Travel\")"; // filter to get section Travel if Sports checked in the other search fragment
            } else if (isMovies) {
                filterFQ = "section_name:(\"Movies\")"; // filter to get section Movies if Sports checked in the other search fragment
            }

            Call<ArticleASArray> call = service.getSearchedArticles(userInput, filterFQ, getString(R.string.api_key)); // calling API
            call.enqueue(new Callback<ArticleASArray>() {
                @Override
                public void onResponse(Call<ArticleASArray> call, Response<ArticleASArray> response) {
                    Button backBtn = v.findViewById(R.id.go_back_btn);
                    try {
                        ArticleASArray articleASArray = response.body();

                        if (articleASArray != null) {
                            listArticle = articleASArray.getArticleASDocs().getArticleAS();
                        }

                        onDataLoaded(); // once the elements are found, we will generate them with the RVAdapter

                        if (articleASArray != null && articleASArray.getArticleASDocs().getArticleAS().size() <= 0) { // prompt error to user in case no results are found
                            Toast.makeText(getContext(), "No results found for this research", Toast.LENGTH_SHORT).show();
                        }

                        backBtn.setOnClickListener(new View.OnClickListener() { // add a button to exit this page
                            @Override
                            public void onClick(View v) {
                                ArticleSearchGraphicFragment fragment = new ArticleSearchGraphicFragment();
                                Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.frameContainer, fragment).addToBackStack(null).commit();
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ArticleASArray> call, Throwable t) {
                    Toast.makeText(getContext(), "Failure to contact the server", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void onDataLoaded() { // we plug the adapter to the RV and make it displays articles fetched in this fragment
        RecyclerViewAdapterAS recyclerAdapterAS = new RecyclerViewAdapterAS(getContext(), listArticle);
        recyclerViewAs.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerViewAs.setAdapter(recyclerAdapterAS);
    }
}