package com.lepanda.studioneopanda.mynewsautopv5.ui.fragments;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.lepanda.studioneopanda.mynewsautopv5.R;

import java.util.Calendar;
import java.util.Objects;

public class ArticleSearchGraphicFragment extends Fragment {

    private View v;

    private EditText userInput;

    private CheckBox businessBox;
    private CheckBox sportsBox;
    private CheckBox artsBox;
    private CheckBox opinionBox;
    private CheckBox moviesBox;
    private CheckBox travelBox;

    private boolean isBusiness = false;
    private boolean isArts = false;
    private boolean isSports = false;
    private boolean isOpinion = false;
    private boolean isMovies = false;
    private boolean isTravel = false;

    private static final String TAG = "ArticleFrag";

    private TextView displayDateBegin;
    private TextView displayDateEnd;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    private DatePickerDialog.OnDateSetListener mDateSetListenerEnd;



    public ArticleSearchGraphicFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Variables initialization
        v = inflater.inflate(R.layout.search_result_graphic, container, false);

        userInput = v.findViewById(R.id.editTextQueryUser);
        Button launchSearch = v.findViewById(R.id.launchSearch);

        displayDateBegin = v.findViewById(R.id.tvDateBegin);
        displayDateEnd = v.findViewById(R.id.tvDate);
        businessBox = v.findViewById(R.id.businessBox);
        sportsBox = v.findViewById(R.id.sportsBox);
        artsBox = v.findViewById(R.id.artsBox);
        opinionBox = v.findViewById(R.id.opinionBox);
        moviesBox = v.findViewById(R.id.moviesBox);
        travelBox = v.findViewById(R.id.travelBox);

        launchSearch.setText(R.string.rechercher_string);

        // Toolbar setup
        android.support.v7.widget.Toolbar toolbar = v.findViewById(R.id.toolbarsearch);
        ((AppCompatActivity) Objects.requireNonNull(getActivity())).setSupportActionBar(toolbar);
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("Search Articles");
            actionBar.setElevation(0);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Objects.requireNonNull(getActivity()).finish(); // to prevent activity to override the fragment layout
            }
        });

        // OnClickListener affected to the "Search" Button of the layout
        launchSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { // Each category has its boolean var affected to false by default, if it's
                                          // check, it becomes true
                if (businessBox.isChecked() || artsBox.isChecked() || sportsBox.isChecked() ||
                opinionBox.isChecked() || moviesBox.isChecked() || travelBox.isChecked()) {
                    if (artsBox.isChecked()) {
                        isArts = true;
                    } else if (sportsBox.isChecked()) {
                        isSports = true;
                    } else if (businessBox.isChecked()) {
                        isBusiness = true;
                    } else if (opinionBox.isChecked()) {
                        isOpinion = true;
                    } else if (moviesBox.isChecked()) {
                        isMovies = true;
                    } else if (travelBox.isChecked()) {
                        isTravel = true;
                    }


                    // We are passing the userInput and BoxChecked bools to the other fragment displaying the results
                    ArticleSearchFragment fragment = new ArticleSearchFragment();
                    Bundle args = new Bundle();
                    args.putString("UserInput", userInput.getText().toString());
                    args.putBoolean("BusinessBoxCheck", isBusiness);
                    args.putBoolean("ArtsBoxCheck", isArts);
                    args.putBoolean("SportsBoxCheck", isSports);
                    args.putBoolean("TravelBoxCheck", isTravel);
                    args.putBoolean("OpinionBoxCheck", isOpinion);
                    args.putBoolean("MoviesBoxCheck", isMovies);
                    fragment.setArguments(args);
                    if (getFragmentManager() != null) {
                        getFragmentManager().beginTransaction().add(R.id.frameContainer, fragment).commit();
                    }

                    Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                            .replace(R.id.frameContainer, fragment).addToBackStack(null).commit();
                } else {
                    Toast.makeText(getActivity(), "You must choose at least one Category", Toast.LENGTH_SHORT).show();
                }
            }
        });

        //setup datepickers
        displayDateBegin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        getContext(),
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        displayDateEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        getContext(),
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListenerEnd,
                        year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                Log.d(TAG, "onDateSet: mm/dd/yyy: " + month + "/" + day + "/" + year);

                String dateBegin = month + "/" + day + "/" + year;
                displayDateBegin.setText(dateBegin);
            }
        };

        mDateSetListenerEnd = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                Log.d(TAG, "onDateSet: mm/dd/yyy: " + month + "/" + day + "/" + year);

                String dateEnd = month + "/" + day + "/" + year;
                displayDateEnd.setText(dateEnd);
            }
        };

        return v;
    }
}
