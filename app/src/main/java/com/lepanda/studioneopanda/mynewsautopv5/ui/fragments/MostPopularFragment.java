package com.lepanda.studioneopanda.mynewsautopv5.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lepanda.studioneopanda.mynewsautopv5.data.model.ArticleMP;
import com.lepanda.studioneopanda.mynewsautopv5.data.model.ArticleMPArray;
import com.lepanda.studioneopanda.mynewsautopv5.data.api.Client;
import com.lepanda.studioneopanda.mynewsautopv5.data.api.NewYorkTimesService;
import com.lepanda.studioneopanda.mynewsautopv5.R;
import com.lepanda.studioneopanda.mynewsautopv5.ui.adapters.RecyclerViewAdapaterMP;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MostPopularFragment extends Fragment {

    private RecyclerView recyclerView;
    private List<ArticleMP> listArticle;

    public MostPopularFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.mostpopular_fragment, container, false);

        recyclerView = v.findViewById(R.id.most_popular_recyclerview);

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        loadData();
    }

    private void loadData() {
        Client client = Client.getINSTANCE();
        NewYorkTimesService service = client.getService();
        Call<ArticleMPArray> call = service.getMostPopularItems(getString(R.string.api_key));

        call.enqueue(new Callback<ArticleMPArray>() {
            @Override
            public void onResponse(@NonNull Call<ArticleMPArray> call, @NonNull Response<ArticleMPArray> response) {
                ArticleMPArray articleMPArray = response.body();

                if (articleMPArray != null) {
                    listArticle = articleMPArray.getArticleMPS();
                }
                onDataLoaded();
            }

            @Override
            public void onFailure(@NonNull Call<ArticleMPArray> call, @NonNull Throwable t) {
                Log.d(getString(R.string.Error), t.getMessage());
            }
        });
    }

    private void onDataLoaded() {
        RecyclerViewAdapaterMP recyclerAdapter = new RecyclerViewAdapaterMP(getContext(), listArticle);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(recyclerAdapter);
    }
}
