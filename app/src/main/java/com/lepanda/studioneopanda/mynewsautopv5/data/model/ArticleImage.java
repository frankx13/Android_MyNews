package com.lepanda.studioneopanda.mynewsautopv5.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ArticleImage {

    @SerializedName("url")
    @Expose
    private String urlImage;

    //CONSTRUCTOR
    public ArticleImage(String urlImage) {
        this.urlImage = urlImage;
    }

    //GETTER
    public String getUrlImage() {
        return urlImage;
    }
}
