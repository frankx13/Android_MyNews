package com.lepanda.studioneopanda.mynewsautopv5.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ArticleArray {

    @SerializedName("results")
    @Expose
    private List<Article> articles;

    //CONSTRUCTOR
    public ArticleArray(List<Article> articles) {
        this.articles = articles;
    }

    //GETTER
    public List<Article> getArticles() {
        return articles;
    }

}