package com.lepanda.studioneopanda.mynewsautopv5.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.lepanda.studioneopanda.mynewsautopv5.ui.fragments.NotificationsGraphicFragment;
import com.lepanda.studioneopanda.mynewsautopv5.R;

public class NotificationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifs);

        // Begin the transaction
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        // Replace the contents of the container with the new fragment
        ft.replace(R.id.frameContainerNotif, new NotificationsGraphicFragment());
        ft.commit();
    }
}