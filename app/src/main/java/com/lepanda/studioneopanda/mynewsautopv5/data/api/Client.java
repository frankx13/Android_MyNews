package com.lepanda.studioneopanda.mynewsautopv5.data.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Client { // we use a Singleton to return an instance of a client and a service

    private static Client INSTANCE = new Client();

    private Retrofit retrofit;

    private Client() { // we pass an URL and we create a new instance of Retrofit with its Builder
        String API_URL = "https://api.nytimes.com/svc/";
        retrofit = new Retrofit.Builder()
                .baseUrl(API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static Client getINSTANCE() {
        return INSTANCE;
    }

    public NewYorkTimesService getService() {
        return retrofit.create(NewYorkTimesService.class);
    }
}