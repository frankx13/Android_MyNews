package com.lepanda.studioneopanda.mynewsautopv5.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Medium {

    @SerializedName("media-metadata")
    @Expose
    private List<ArticleMPImage> mediaMetadata;

    //CONSTRUCTOR
    public Medium(List<ArticleMPImage> mediaMetadata) {
        this.mediaMetadata = mediaMetadata;
    }

    //GETTER
    public List<ArticleMPImage> getMediaMetadata() {
        return mediaMetadata;
    }
}