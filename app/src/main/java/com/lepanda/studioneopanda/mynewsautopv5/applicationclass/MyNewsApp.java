package com.lepanda.studioneopanda.mynewsautopv5.applicationclass;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;

public class MyNewsApp extends Application {

    public static final String CHANNEL_1_ID = "channel1";

    @Override
    public void onCreate() {
        super.onCreate();

        createNotificationsChannels(); // we call the method responsible of creating the channel
    }

    private void createNotificationsChannels() { // we create a notification channel to use for the Notifications
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {  // O is for Oreo = API 26
            NotificationChannel channel1 = new NotificationChannel(
                    CHANNEL_1_ID,
                    "Channel 1", //ch1
                    NotificationManager.IMPORTANCE_HIGH
            );
            channel1.setDescription("This is Channel 1");


            NotificationManager manager = getSystemService(NotificationManager.class);
            if (manager != null) {
                manager.createNotificationChannel(channel1); // if an instance of manager exists, we create a new createNotificationChannel
            }
        }
    }
}
