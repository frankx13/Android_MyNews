package com.lepanda.studioneopanda.mynewsautopv5.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ArticleASImage {

    @SerializedName("url")
    @Expose
    private String url;

    //CONSTRUCTOR
    public ArticleASImage(String url) {
        this.url = url;
    }

    //GETTER
    public String getUrl() {
        return url;
    }
}
