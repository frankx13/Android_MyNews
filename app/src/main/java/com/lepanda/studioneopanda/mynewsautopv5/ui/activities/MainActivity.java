package com.lepanda.studioneopanda.mynewsautopv5.ui.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.lepanda.studioneopanda.mynewsautopv5.ui.fragments.BusinessFragment;
import com.lepanda.studioneopanda.mynewsautopv5.ui.fragments.MostPopularFragment;
import com.lepanda.studioneopanda.mynewsautopv5.ui.fragments.TopStoriesFragment;
import com.lepanda.studioneopanda.mynewsautopv5.R;
import com.lepanda.studioneopanda.mynewsautopv5.ui.adapters.ViewPagerAdapter;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;

    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TabLayout tabLayout = findViewById(R.id.tablayout_id);
        viewPager = findViewById(R.id.viewpager_id);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        //Fragments

        adapter.AddFragment(new TopStoriesFragment(), "TopStories");
        adapter.AddFragment(new MostPopularFragment(), "Most Popular");
        adapter.AddFragment(new BusinessFragment(), "Business");

        //ViewPager setup Frags
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        Objects.requireNonNull(tabLayout.getTabAt(0)).setTag("p1").setIcon(R.drawable.ic_topstoriesapi);
        Objects.requireNonNull(tabLayout.getTabAt(1)).setTag("p2").setIcon(R.drawable.ic_mostpopularapi);
        Objects.requireNonNull(tabLayout.getTabAt(2)).setTag("p3").setIcon(R.drawable.ic_business);

        //Set toolbar and NavDra
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("My News");
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
            actionBar.setElevation(0);
        }

        mDrawerLayout = findViewById(R.id.drawer_layout);
        NavigationView nvDrawer = findViewById(R.id.nav_view);
        setupDrawerContent(nvDrawer);

    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    public void selectDrawerItem(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.nav_topstories:
                viewPager.setCurrentItem(0);
                break;
            case R.id.nav_mostpopular:
                viewPager.setCurrentItem(1);
                break;
            case R.id.nav_Business:
                viewPager.setCurrentItem(2);
                break;
            default:
                viewPager.setCurrentItem(0);
        }
        // Highlight the selected item has been done by NavigationView
        menuItem.setChecked(true);
        // Set action bar title
        setTitle(menuItem.getTitle());
        // Close the navigation drawer
        mDrawerLayout.closeDrawers();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;

            case R.id.notifications:
                startActivity(new Intent(this, NotificationActivity.class));
                return true;

            case R.id.help:
                Toast.makeText(this, "Need help ? Send us a mail!", Toast.LENGTH_LONG).show();
                return true;

            case R.id.about:
                Toast.makeText(this, "App developped by StudioNeoPanda in 2019", Toast.LENGTH_SHORT).show();
                return true;

            case R.id.search:
                startActivity(new Intent(this, SearchArticleActivity.class));
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);

        return true;
    }
}