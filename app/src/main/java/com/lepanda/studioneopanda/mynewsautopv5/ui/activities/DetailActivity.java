package com.lepanda.studioneopanda.mynewsautopv5.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

import com.lepanda.studioneopanda.mynewsautopv5.R;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        String url = getIntent().getStringExtra("URL_ARTICLE"); // We fetch the URL of article returned by the POJO and
                                                                     // sent by the RV
        WebView webView = findViewById(R.id.mywebview);
        webView.loadUrl(url); // we load the returned URL inside this webview
    }
}
