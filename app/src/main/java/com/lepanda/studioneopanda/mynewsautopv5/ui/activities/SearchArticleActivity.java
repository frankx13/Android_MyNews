package com.lepanda.studioneopanda.mynewsautopv5.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.lepanda.studioneopanda.mynewsautopv5.ui.fragments.ArticleSearchGraphicFragment;
import com.lepanda.studioneopanda.mynewsautopv5.R;

public class SearchArticleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.frameContainer, new ArticleSearchGraphicFragment());
        ft.commit();
    }
}