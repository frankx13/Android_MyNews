package com.lepanda.studioneopanda.mynewsautopv5.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.lepanda.studioneopanda.mynewsautopv5.R;
import com.lepanda.studioneopanda.mynewsautopv5.jobs.NotifyWorker;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import androidx.work.Data;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

public class NotificationsGraphicFragment extends Fragment {

    private CheckBox businessboxNotif;
    private CheckBox artsBoxNotif;
    private CheckBox sportsBoxNotif;
    private CheckBox moviesBoxNotif;
    private CheckBox opinionBoxNotif;
    private CheckBox travelBoxNotif;


    private Switch switchNotification;
    private TextView notificationTextNotif;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.notifs_graphic_fragment, container, false);

        businessboxNotif = v.findViewById(R.id.businessNotif);
        artsBoxNotif = v.findViewById(R.id.artsNotif);
        sportsBoxNotif = v.findViewById(R.id.sportsNotif);
        moviesBoxNotif = v.findViewById(R.id.moviesNotif);
        opinionBoxNotif = v.findViewById(R.id.opinionNotif);
        travelBoxNotif = v.findViewById(R.id.travelNotif);

        switchNotification = v.findViewById(R.id.switchNotification);
        notificationTextNotif = v.findViewById(R.id.notificationTextNotif);

        android.support.v7.widget.Toolbar toolbar = v.findViewById(R.id.toolbarsearchNotif);
        ((AppCompatActivity) Objects.requireNonNull(getActivity())).setSupportActionBar(toolbar);
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("Notifications");
            actionBar.setElevation(0);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Objects.requireNonNull(getActivity()).finish();
            }
        });
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        switchNotification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (!businessboxNotif.isChecked() && !artsBoxNotif.isChecked() && !sportsBoxNotif.isChecked()
                && !opinionBoxNotif.isChecked() && !travelBoxNotif.isChecked() && !moviesBoxNotif.isChecked()) {
                    notificationTextNotif.setText(R.string.tick_box_warning);
                } else {
                    String filterGivenToWorker = "";

                    if (switchNotification.isChecked()) {
                        if (businessboxNotif.isChecked()) {
                            filterGivenToWorker = "section_name:(\"Business\")";
                        } else if (artsBoxNotif.isChecked()) {
                            filterGivenToWorker = "section_name:(\"Arts\")";
                        } else if (sportsBoxNotif.isChecked()) {
                            filterGivenToWorker = "section_name:(\"Sports\")";
                        } else if (opinionBoxNotif.isChecked()){
                            filterGivenToWorker = "section_name:(\"Opinion\")";
                        } else if (travelBoxNotif.isChecked()){
                            filterGivenToWorker = "section_name:(\"Travel\")";
                        } else if (moviesBoxNotif.isChecked()){
                            filterGivenToWorker = "section_name:(\"Movies\")";
                        }

                        switchNotification.setVisibility(View.VISIBLE);
                        notificationTextNotif.setText(R.string.message_notif_enabled);

                        Data myData = new Data.Builder()
                                .putString(getString(R.string.filterOfAPI), filterGivenToWorker)
                                .build();

                        PeriodicWorkRequest periodicWorkRequest = new PeriodicWorkRequest.Builder(
                                NotifyWorker.class,
                                24, TimeUnit.HOURS)
                                .setInputData(myData)
                                .build();
                        WorkManager.getInstance().enqueue(periodicWorkRequest);
                    } else {
                        notificationTextNotif.setText(R.string.message_notif_disabled);
                        // dequeue the periodicworker
                        WorkManager.getInstance().cancelAllWork();
                    }
                }
            }
        });
    }
}