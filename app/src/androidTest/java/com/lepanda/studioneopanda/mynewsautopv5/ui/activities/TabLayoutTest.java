package com.lepanda.studioneopanda.mynewsautopv5.ui.activities;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.lepanda.studioneopanda.mynewsautopv5.R;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.assertThat;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.core.IsNull.notNullValue;

@RunWith(AndroidJUnit4.class)
public class TabLayoutTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    @Before
    public void setUp() {
        MainActivity mActivity = mActivityRule.getActivity();
        assertThat(mActivity, notNullValue());
    }

    @Test
    public void swipePage() { // we check that the swipe on the screen is working
        onView(withId(R.id.viewpager_id))
                .check(matches(isDisplayed()));

        onView(withId(R.id.viewpager_id))
                .perform(swipeLeft());
    }

    @Test
    public void clickPage() { // we check that the click on a tab is working
        onView(withId(R.id.viewpager_id))
                .check(matches(isDisplayed()));

        onView(withId(R.id.viewpager_id))
                .perform(click());
    }
}